package com.soc.course.sercice;

import com.baomidou.mybatisplus.extension.service.IService;
import com.soc.course.entity.Task;

import java.util.HashMap;


public interface TaskService extends IService<Task> {

}
