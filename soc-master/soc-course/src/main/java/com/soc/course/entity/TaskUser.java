package com.soc.course.entity;

import lombok.Data;

import java.util.Date;
@Data
public class TaskUser {
    private String id;
    private String taskId;
    private String userId;
    private String no;
    private String courseId;
    private String classId;
    private String chapterId;
    private Date completeTime;


    // getters and setters
}
