package com.soc.course.mapper;



import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.soc.course.entity.ChapterDetail;
import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface ChapterDetailMapper extends BaseMapper<ChapterDetail> {
}

