package com.soc.course.sercice.resEntity;


import com.soc.course.entity.Chapter;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
@Data
public class ChapterVO {

    private String chapterId;

    private String chapterTitle;

    private String courseId;

    private String parentId;

    private Integer rank;

    private Integer sortOrder;

    private Integer childCount;

    private List<ChapterVO> children = new ArrayList<>();

    public ChapterVO(Chapter chapter) {
        this.chapterId = chapter.getChapterId();
        this.chapterTitle = chapter.getChapterTitle();
        this.courseId = chapter.getCourseId();
        this.parentId = chapter.getParentId();
        this.rank = chapter.getRank();
        this.sortOrder = chapter.getSortOrder();
        this.childCount = chapter.getChildCount();
    }

    // 省略getter和setter方法

    /**
     * 添加子章节
     * @param chapterVO 子章节
     */
    public void addChild(ChapterVO chapterVO) {
        children.add(chapterVO);
    }
}
