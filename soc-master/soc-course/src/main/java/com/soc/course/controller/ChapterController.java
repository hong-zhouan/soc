package com.soc.course.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.soc.course.entity.Chapter;
import com.soc.course.entity.ChapterDetail;
import com.soc.course.entity.TaskUser;
import com.soc.course.sercice.ChapterDetailService;
import com.soc.course.sercice.ChapterService;
import com.soc.course.sercice.CourseService;
import com.soc.course.sercice.TaskUserService;
import com.soc.course.sercice.resEntity.ChapterNode;
import com.soc.file.servie.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName ChapterController
 * @Description ToDo
 * @Author hjy
 * @Date 2023/4/22 23:30
 **/

@RestController
@RequestMapping("/chapter")
public class ChapterController {

    @Autowired
    private ChapterService chapterService;
    @Autowired
    private CourseService courseService;

    @Autowired
    private ChapterDetailService chapterDetailService;

    @Autowired
    private TaskUserService taskUserService;

    @GetMapping()
    public R getChapterByCourseId(@RequestParam String courseId, @RequestParam String userId) {
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("chapterList", chapterService.getChapterList(courseId, userId));
        resultMap.put("courseInfo", courseService.getById(courseId));
        return new R(200, "", resultMap);
    }

    @PostMapping()
    public R creatChapter(@RequestBody JSONObject jsonObject) {
        String siblingChapterId = jsonObject.getString("siblingChapterId");
        String parentId = jsonObject.getString("parentId");
        String chapterTitle = jsonObject.getString("chapterTitle");
        String courseId = jsonObject.getString("courseId");
        Chapter chapter = new Chapter();
        chapter.setChapterTitle(chapterTitle);
        if (siblingChapterId == null) {
            if (chapterService.addChildChapter(parentId, courseId, chapter)) {
                return new R(200, "添加成功", chapter);
            } else return new R(0, "添加失败");
        } else {
            if (chapterService.addSiblingChapterBefore(parentId, courseId, siblingChapterId, chapter)) {
                return new R(200, "添加成功", chapter);
            } else return new R(0, "添加失败");
        }
    }

    @DeleteMapping()
    public R delChapterById(@RequestParam String chapterId) {
        if (chapterService.deleteChapterById(chapterId)) {
            return new R(200, "删除成功");
        } else return new R(0, "删除失败");
    }

    @PostMapping("/updateTitle")
    public R updateChapterById(@RequestBody Chapter chapter) {
        if (chapterService.updateChapterTitleById(chapter.getChapterId(), chapter.getChapterTitle())) {
            return new R(200, "更新成功");
        } else return new R(0, "更新失败");
    }


    @GetMapping("/detail")
    public R getDetail(@RequestParam String chapterId, String userId) {
        List<ChapterDetail> chapterDetail = chapterDetailService.getDetailByChaterId(chapterId, userId);
        if (chapterDetail == null) {
            return new R(0, "结果为空,请检查参数是否正确");
        } else {
            return new R(200, "获取成功", chapterDetail);
        }
    }


    @PostMapping("/detail")
    public R addDetail(@RequestBody ChapterDetail chapterDetail) {
        System.out.println(chapterDetail);
        if (chapterDetail.getSortOrder() == null) {
            if (chapterDetailService.addChapterDetail(chapterDetail)) {
                return new R(200, "添加成功");
            } else return new R(0, "添加失败");
        } else {
            if (chapterDetailService.insertChapterDetail(chapterDetail, chapterDetail.getSortOrder())) {
                return new R(200, "添加成功");
            } else return new R(0, "添加失败");
        }
    }

    @PutMapping("/detail")
    public R updateDetail(@RequestBody ChapterDetail chapterDetail) {
            chapterDetail.setIsFinshed(true);
        if (chapterDetailService.updateById(chapterDetail)) {
            return new R(200, "更新成功");
        } else return new R(0, "更新失败");
    }


    @PutMapping("/detail/list")
    public R updateAllDetail(@RequestBody List<ChapterDetail> chapterDetailList) throws Exception {
        if(chapterDetailList==null){
            throw new Exception("不可为空");
        }
        String ChapterId = chapterDetailList.get(0).getChapterId();
        LambdaQueryWrapper<ChapterDetail> chapterLambdaQueryWrapper = new LambdaQueryWrapper<>();
        chapterLambdaQueryWrapper.eq(ChapterDetail::getChapterId,ChapterId);
        chapterDetailService.remove(chapterLambdaQueryWrapper);
        if (chapterDetailService.saveOrUpdateBatch(chapterDetailList)){
            return new R(200,"批量更新成功");
        }
        return new R(0, "批量更新失败");
    }


    @DeleteMapping("/detail")
    public R deleteDetail(@RequestParam String chapterDetailId) {
        if (chapterDetailService.deleteChapterDetail(chapterDetailId)) {
            return new R(200, "删除成功");
        } else return new R(0, "删除失败");
    }


    @GetMapping("/task")
    public R getCountTaskCount(@RequestParam String courseId, @RequestParam String userId) {
        Integer courseTotalTask = chapterService.getCourseTask(courseId);
        Integer userTask = taskUserService.getUserCourseTask(courseId, userId);
        HashMap<String, Integer> map = new HashMap<>();
        map.put("courseTotalTask", courseTotalTask);
        map.put("userTask", userTask);
        return new R(200, "获取成功", map);
    }


    /**
     * TODO 问题课程
     * @param   taskUser
     * @return
     */

    @PostMapping("/task")
    public R addUserTask(@RequestBody TaskUser taskUser) {
        if (taskUserService.updateTaskToComplete(taskUser)) {
            return new R(200, "添加成功");
        } else {
            return new R(0, "添加失败");
        }
    }


    @GetMapping("/task/class")
    public R getChapterByClass(@RequestParam String classId, @RequestParam String courseId) {
        return new R(200, "", chapterService.getChapterByClassId(classId, courseId));
    }




}
