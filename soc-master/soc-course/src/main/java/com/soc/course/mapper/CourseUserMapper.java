package com.soc.course.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.soc.course.entity.CourseUser;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CourseUserMapper extends BaseMapper<CourseUser> {
}

