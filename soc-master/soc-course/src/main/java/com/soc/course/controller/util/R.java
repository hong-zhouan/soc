package com.soc.course.controller.util;

import lombok.Data;

import java.util.ArrayList;

@Data
public class R {
    private Integer code;
    private String msg="";
    private Object data=new ArrayList<>();

    public R() {
    }

    public R(Integer code) {
        this.code = code;
    }

    public R(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public R(Integer code, Object data) {
        this.code = code;
        this.msg="";
        this.data =data;
    }
    public R(Integer code, String msg,Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

}
