package com.soc.course.sercice;

import com.baomidou.mybatisplus.extension.service.IService;
import com.soc.course.entity.CourseUser;


import java.util.List;

public interface CourseUserService extends IService<CourseUser> {

    boolean addUserCourse(String userId, String courseId, String classId, Integer type);

    boolean deleteUserCourseById(String id);

    boolean addBatchUserCourse(List<CourseUser> courseUserList);



    boolean courseAddStudents(List<String> studentIds, String ClassId,String courseId);

    List<String> getCourseIdsByUser(String userId, Integer type);

    boolean deleteUserCourseByCourseId(String courseId);
}
