package com.soc.course.sercice;

import com.baomidou.mybatisplus.extension.service.IService;
import com.soc.course.entity.Meal;


public interface MealService extends IService<Meal> {

}
