package com.soc.course.entity;

import lombok.Data;

import java.util.Date;

@Data
public class Task {
    private String taskId;
    private String taskName;
    private String isTask;
    private String chapterId;
    private Date createTime;
    private String courseId;

    // getters and setters
}
