package com.soc.course.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import com.soc.course.controller.util.R;
import com.soc.course.entity.Course;
import com.soc.course.entity.CourseUser;
import com.soc.course.entity.Meal;
import com.soc.course.entity.MealCourse;
import com.soc.course.mapper.MealCourseMapper;
import com.soc.course.sercice.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @ClassName CourseController
 * @Description T
 * @Author hjy
 * @Date 2023/4/23 1:36
 **/
@RestController
@RequestMapping("/course")
public class CourseController {
    @Autowired
    private CourseService courseService;
    @Autowired
    private CourseUserService courseUserService;
    @Autowired
    private ChapterService chapterService;

    @Autowired
    private MealService mealService;

    @Autowired
    private MealCourseService mealCourseService;

    @Autowired
    private MealCourseMapper mealCourseMapper;

    @GetMapping
    public R getCourseByUserId(@RequestParam String userId, @RequestParam Integer type) {
        List<String> courseIds = courseUserService.getCourseIdsByUser(userId, type);
        if (courseIds.size() > 0) {
            List<Course> courseList = courseService.getCourseListByIds(courseIds);
            return new R(200, "查询成功", courseList);
        } else {
            return new R(200, "查询成功", new ArrayList<>());
        }

    }

    /**
     * @Author hjy
     * @Description //TODO 老师创建课程
     * @Date 2:40 2023/4/23
     * @Param userId:
     * @Param course:
     * @return: com.hjy.soc.course.controller.util.R
     **/
    @PostMapping("/teacherCreate/{userId}")
    public R addCourseByteacher(@PathVariable String userId, @RequestBody Course course) {
        if (courseService.addCourse(course)) {
            CourseUser courseUser = new CourseUser();
            if (courseUserService.addUserCourse(userId, course.getCourseId(), "0", 0)) {
                return new R(200, "创建成功", course);
            } else {
                return new R(0, "课程添加成功,绑定失败");
            }
        } else return new R(0, "课程创建失败");
    }


    @PostMapping("/update")
    public R updateBytecher(@RequestBody Course course) {
        System.out.println(course);
        boolean b = courseService.updateCourse(course);
        if (b) return new R(200, "修改成功", course);
        else return new R(0, "修改课程失败");

    }

    @PostMapping("/importStudent")
    public R courseImportStudentByClassId(@RequestBody JSONObject jsonObject) {
        String classId = jsonObject.getString("classId");
        String courseId = jsonObject.getString("courseId");
        // 需要根据班级id调用userService的接口,获取班级内学生id
        List<String> studentIds = new ArrayList<>();
        courseUserService.courseAddStudents(studentIds, classId, courseId);
        return new R();
    }

    /**
     * @Author hjy
     * @Description //
     * TODO 获取公共课    问题课程
     * @Date 23:30 2023/6/25
     * @Param currentPage: 当前页
     * @Param pageSize: 每页大小
     * @return: com.soc.course.controller.util.R
     **/
    @GetMapping("/publicCourse")
    public R getPublicCourse(@RequestParam Integer currentPage, @RequestParam Integer pageSize) {
        return new R(200, courseService.getPage(currentPage, pageSize));
    }

    /**
     * @Author hjy
     * @Description //TODO 教师删除课程
     * @Date 2:11 2023/4/23
     * @Param courseId:
     * @Param userId:
     * @return: com.hjy.soc.course.controller.util.R
     **/

    @DeleteMapping()
    public R DeleteCourseByIdList(@RequestBody JSONObject jsonObject) {
        JSONArray courseIdList = jsonObject.getJSONArray("courseIdList");
        // 删除课程对应的章节,删除 课程与老师,学生之间的关联,删除对应的课程信息
        if (chapterService.removeBatchByIds(courseIdList) && courseUserService.removeBatchByIds(courseIdList) && courseService.removeBatchByIds(courseIdList)) {
            return new R(200, "删除成功");
        } else return new R(0, "删除失败");
    }

    @PostMapping("/student/addCourse")
    public R addStudentCourse(@RequestBody CourseUser courseUser) {
        boolean save = courseUserService.save(courseUser);
        if (save) {
            return new R(200, "添加成功", courseUser);
        } else {
            return new R(0, "课程添加失败");
        }
    }

}
