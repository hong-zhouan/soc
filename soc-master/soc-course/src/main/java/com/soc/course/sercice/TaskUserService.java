package com.soc.course.sercice;

import com.baomidou.mybatisplus.extension.service.IService;
import com.soc.course.entity.TaskUser;

import java.util.Map;


public interface TaskUserService extends IService<TaskUser> {

    Integer getUserCourseTask(String courseId, String userId);

    Map<String, Long> getCourseTaskMap(String CourseId, String userId);

    Map<String, Long> getTaskByClassID(String courseId, String classId);

    Map<String,Long> getUserFinshTask(String userId, String classId);

    Map<String, Boolean> getChapterFinshTask(String chapterId, String userId);

    boolean updateTaskToComplete(TaskUser taskUser);
}
