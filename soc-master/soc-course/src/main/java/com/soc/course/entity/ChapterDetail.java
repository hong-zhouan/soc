package com.soc.course.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data
public class ChapterDetail {
    private String id;
    private String chapterId;
    private String chapterDetail;
    private String resourceUrl;
    private Boolean isVideo;
    private Integer sortOrder;
    // getters and setters
    private Boolean isTask;
    @JsonIgnore
    private Boolean isDelete;
    private String courseId;
//    @TableField(exist = false)
    private Boolean isFinshed;
}
