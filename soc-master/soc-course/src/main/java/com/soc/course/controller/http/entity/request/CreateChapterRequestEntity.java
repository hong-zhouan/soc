package com.soc.course.controller.http.entity.request;

import lombok.Data;

/**
 * @ClassName CreateChapterRequestEntity
 * @Description ToDo
 * @Author hjy
 * @Date 2023/4/22 23:45
 **/
@Data
public class CreateChapterRequestEntity {
    private String courseId;
    private String chapterTitle;
    private String parentId;
    private String siblingChapterId;
}
