package com.soc.course.sercice.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.soc.course.entity.Task;
import com.soc.course.mapper.TaskMapper;
import com.soc.course.sercice.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
public class TaskServiceImpl extends ServiceImpl<TaskMapper, Task> implements TaskService {


}
