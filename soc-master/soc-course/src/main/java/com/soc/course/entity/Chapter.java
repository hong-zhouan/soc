package com.soc.course.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

@Data
public class Chapter {
    @TableId(value="chapter_id",type = IdType.ASSIGN_ID)
    private String chapterId;
    private String chapterTitle;
    private String courseId;
    private String parentId;
    private Integer rank;
    private Integer sortOrder;
    private Integer childCount;
    private Integer resourceCount;
    private Integer videoCount;
    
    // getters and setters
}
