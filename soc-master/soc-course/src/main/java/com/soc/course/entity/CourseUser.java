package com.soc.course.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

@Data
public class CourseUser {
    @TableId(type = IdType.ASSIGN_ID)
    private String courseUserId;
    private String userId;
    private String courseId;
    private String classId;
//     类型 0是教,1是学
    private Integer type;
    // getters and setters
}
