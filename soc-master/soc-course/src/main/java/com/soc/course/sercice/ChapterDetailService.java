package com.soc.course.sercice;

import com.baomidou.mybatisplus.extension.service.IService;
import com.soc.course.entity.ChapterDetail;
import org.springframework.stereotype.Service;


import java.util.List;

@Service
public interface ChapterDetailService extends IService<ChapterDetail> {

    List<ChapterDetail> getDetailByChaterId(String chapterID,String userId);

    boolean insertChapterDetail(ChapterDetail chapterDetail, Integer sortOder);

    boolean addChapterDetail(ChapterDetail chapterDetail);

    boolean deleteChapterDetail(String chapterDetailId);
}
