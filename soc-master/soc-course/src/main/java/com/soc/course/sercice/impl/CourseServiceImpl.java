package com.soc.course.sercice.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.soc.course.entity.Chapter;
import com.soc.course.entity.Course;
import com.soc.course.mapper.CourseMapper;
import com.soc.course.sercice.ChapterService;
import com.soc.course.sercice.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CourseServiceImpl extends ServiceImpl<CourseMapper, Course> implements CourseService {
    @Autowired
    private ChapterService chapterService;

    @Autowired
    private CourseMapper courseMapper;
    /**
     * 批量查询课程
     *
     * @param courseIds 课程ID列表
     * @return 课程列表
     */
    @Override
    public List<Course> getCourseListByIds(List<String> courseIds) {
        return list(new QueryWrapper<Course>().in("course_id", courseIds));
    }

    /**
     * 修改课程
     *
     * @param course 待修改的课程
     * @return 是否修改成功
     */
    @Override
    public boolean updateCourse(Course course) {
        return updateById(course);
    }

    /**
     * 批量删除课程
     *
     * @param courseIds 待删除的课程ID列表
     * @return 是否删除成功
     */
    @Override
    public boolean deleteCourses(List<String> courseIds) {
        return removeBatchByIds(courseIds);
    }

    /**
     * 添加课程
     *
     * @param course 待添加的课程
     * @return 是否添加成功
     */
    @Override
    public boolean addCourse(Course course) {
        Chapter chapter = new Chapter();
        chapter.setChapterTitle("新建目录");
        return save(course)&&chapterService.addChildChapter("0",course.getCourseId(),chapter);
    }

    /**
     * @param couseId 待添加的课程id
     * @return boolean
     **/
    @Override
    public boolean deleteCourseById(String couseId) {
        return removeById(couseId);
    }


    @Override
    public IPage<Course> getPage(int currentPage, int pageSize) {
        IPage page = new Page(currentPage, pageSize);
        LambdaQueryWrapper<Course> lqw = new LambdaQueryWrapper<>();
        lqw.eq(Course::getIsPublic,1);
        courseMapper.selectPage(page,lqw);
        return page;
    }
}
