package com.soc.course.sercice.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.soc.course.entity.Chapter;
import com.soc.course.entity.ChapterDetail;
import com.soc.course.mapper.ChapterDetailMapper;
import com.soc.course.sercice.ChapterDetailService;
import com.soc.course.sercice.ChapterService;
import com.soc.course.sercice.TaskUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class ChapterDetailServiceImpl extends ServiceImpl<ChapterDetailMapper, ChapterDetail> implements ChapterDetailService {

    @Autowired
    private ChapterDetailMapper chapterDetailMapper;

    @Autowired
    private ChapterService chapterService;
    @Autowired
    private TaskUserService taskUserService;

    //    @Override
//    public List<ChapterDetail> getDetailByChaterId(String chapterID){
//        LambdaQueryWrapper<ChapterDetail> queryWrapper = new LambdaQueryWrapper<ChapterDetail>();
//        queryWrapper.eq(ChapterDetail::getChapterId,chapterID);
//        return chapterDetailMapper.selectList(queryWrapper);
//    }
    @Override
    public List<ChapterDetail> getDetailByChaterId(String chapterID, String userId) {
        QueryWrapper<ChapterDetail> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("chapter_id", chapterID);
        queryWrapper.orderBy(true, true, "sort_order");
        List<ChapterDetail> chapterDetailList = chapterDetailMapper.selectList(queryWrapper);
        Map<String, Boolean> chapterFinshTask = taskUserService.getChapterFinshTask(chapterID, userId);
        for (ChapterDetail chapterDetail : chapterDetailList) {
            boolean isFinshed = chapterFinshTask.getOrDefault(chapterDetail.getId(), Boolean.FALSE);
            chapterDetail.setIsFinshed(isFinshed);
        }
        return chapterDetailList;
    }


    @Override
    public boolean insertChapterDetail(ChapterDetail chapterDetail, Integer sortOder) {
        Chapter chapter = chapterService.getById(chapterDetail.getChapterId());
        chapterDetail.setCourseId(chapter.getCourseId());
        Integer count = chapter.getResourceCount();
        chapter.setResourceCount(count + 1);
        if (chapterDetail.getIsVideo()) {
            chapter.setVideoCount(chapter.getVideoCount() + 1);
        }
        return updateSortOrder(chapter, sortOder, 1) && save(chapterDetail) && chapterService.updateById(chapter);
    }

    @Override
    public boolean addChapterDetail(ChapterDetail chapterDetail) {
        Chapter chapter = chapterService.getById(chapterDetail.getChapterId());
        Integer count = chapter.getResourceCount();
        chapterDetail.setSortOrder(count + 1);
        chapterDetail.setCourseId(chapter.getCourseId());
        chapter.setResourceCount(count + 1);
        if (chapterDetail.getIsVideo()) {
            chapter.setVideoCount(chapter.getVideoCount() + 1);
        }
        return saveOrUpdate(chapterDetail) && chapterService.updateById(chapter);
    }

    @Override
    public boolean deleteChapterDetail(String chapterDetailId) {
        ChapterDetail chapterDetail = getById(chapterDetailId);
        Chapter chapter = chapterService.getById(chapterDetail.getChapterId());
        Integer count = chapter.getResourceCount();
        chapter.setResourceCount(count - 1);
        if (chapterDetail.getIsVideo()) {
            chapter.setVideoCount(chapter.getVideoCount() - 1);
        }
        if (chapterDetail.getSortOrder().equals(count)) {
            return removeById(chapterDetailId) && chapterService.updateById(chapter);
        } else {
            return removeById(chapterDetailId) && chapterService.updateById(chapter) && updateSortOrder(chapter, chapterDetail.getSortOrder(), -1);
        }
    }

    public boolean updateSortOrder(Chapter chapter, Integer sortOrder, Integer isDelete) {
        LambdaQueryWrapper<ChapterDetail> queryChainWrapper = new LambdaQueryWrapper<>();
        queryChainWrapper.eq(ChapterDetail::getChapterId, chapter.getChapterId());
        queryChainWrapper.ge(ChapterDetail::getSortOrder, sortOrder);
        List<ChapterDetail> chapterDetailList = chapterDetailMapper.selectList(queryChainWrapper);
        for (ChapterDetail chapterDetail1 : chapterDetailList) {
            chapterDetail1.setSortOrder(chapterDetail1.getSortOrder() + isDelete);
        }
        return updateBatchById(chapterDetailList);
    }


}
