package com.soc.course.sercice;

import com.baomidou.mybatisplus.extension.service.IService;
import com.soc.course.entity.MealCourse;

public interface MealCourseService extends IService<MealCourse> {

}
