package com.soc.course.sercice.resEntity;

import lombok.Data;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Data
public class ChapterNode {
    private String chapterId;
    private String chapterTitle;
    private String parentId;
    private int rank;
    private int sortOrder;
    private Integer resourceCount;
    private Integer videoCount;
    private List<ChapterNode> children;
    private int taskCount;
    private double completionRate;

    // 构造函数和 getter/setter 方法省略
    public ChapterNode(String chapterId, String chapterTitle, String parentId, Integer sortOrder, Integer resourceCount, Integer videoCount,Integer rank) {
        this.chapterId = chapterId;
        this.chapterTitle = chapterTitle;
        this.parentId = parentId;
        this.children = new ArrayList<>();
        this.sortOrder = sortOrder;
        this.resourceCount = resourceCount;
        this.videoCount = videoCount;
        this.completionRate = 0d;
        this.rank = rank;
    }

    public void addChild(ChapterNode node) {
        this.children.add(node);
    }

    public void sortChildren() {
        if (children == null || children.isEmpty()) {
            return;
        }
        children.sort(Comparator.comparingInt(ChapterNode::getSortOrder));
        for (ChapterNode child : children) {
            child.sortChildren();
        }
    }

    public boolean hasChildren() {
        return this.children != null && this.children.size() != 0;
    }
}