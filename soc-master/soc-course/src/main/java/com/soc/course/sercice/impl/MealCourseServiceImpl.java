package com.soc.course.sercice.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.soc.course.entity.MealCourse;
import com.soc.course.mapper.MealCourseMapper;
import com.soc.course.sercice.MealCourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MealCourseServiceImpl extends ServiceImpl<MealCourseMapper, MealCourse> implements MealCourseService {
    @Autowired
    private MealCourseMapper mealCourseMapper;

}
