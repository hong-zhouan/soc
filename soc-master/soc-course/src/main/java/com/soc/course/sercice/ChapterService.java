package com.soc.course.sercice;

import com.baomidou.mybatisplus.extension.service.IService;
import com.soc.course.entity.Chapter;
import com.soc.course.entity.ChapterDetail;
import com.soc.course.sercice.resEntity.ChapterNode;
import org.springframework.stereotype.Service;


import java.util.List;

@Service
public interface ChapterService extends IService<Chapter> {

    List<ChapterNode> getChapterList(String courseId,String userId);


    boolean addChildChapter(String parentId, String courseId, Chapter child);

    boolean addSiblingChapterBefore(String parentId, String courseId, String siblingChapterId, Chapter newChapter);

    boolean updateChapterTitleById(String chapterId, String newTitle);

    boolean deleteChapterById(String chapterId);

    boolean deleteChapterByCourseId(String courseId);

    Integer getCourseTask(String courseId);

    List<ChapterNode> getChapterByClassId(String classId, String courseId);




}
