package com.soc.course.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.soc.course.controller.util.R;
import com.soc.course.entity.Course;
import com.soc.course.entity.Meal;
import com.soc.course.entity.MealCourse;
import com.soc.course.mapper.MealCourseMapper;
import com.soc.course.mapper.MealMapper;
import com.soc.course.sercice.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @ClassName MealController
 * @Description T
 * @Author yjh
 * @Date 2023/6/28 1:36
 **/
@RestController
@RequestMapping("/course")
public class MealController {

    @Autowired
    private CourseService courseService;
    @Autowired
    private CourseUserService courseUserService;
    @Autowired
    private ChapterService chapterService;

    @Autowired
    private MealService mealService;

    @Autowired
    private MealCourseService mealCourseService;

    @Autowired
    private MealCourseMapper mealCourseMapper;

    @Autowired
    private MealMapper mealeMapper1;



    //查询所有课程
    @PostMapping("/courseall")
    public R ListAllCourse(){
        List<Course> list = courseService.list(null);
        return new R(200,"查询成功",list);
    }


    @PostMapping("/sreachcourse")
    public R ListAllCourse(@RequestParam String coursename) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("course_name", coursename);
        List<Course> courses = courseService.listByMap(map);
        return new R(200, "查询成功", courses);
    }


    //添加课程套餐
    @PostMapping("/addcouresemeal")
    public  R AddCourseMeal (
            @RequestParam String mealName ,
            @RequestParam String mealImgUrl,
            @RequestParam String mealDetail,
            @RequestParam String intrduction,
            @RequestParam String creat_teacher
    ){

        Meal meal =new Meal();
        meal.setMealName(mealName);
        meal.setMealImgUrl(mealImgUrl);
        meal.setMealDetail(mealDetail);
        meal.setIntrduction(intrduction);
        meal.setCreatTeacher(creat_teacher);

        if (mealService.saveOrUpdate(meal)) {
            return new R(200, "添加成功", meal);
        } else return new R(0, "添加失败");
    }


    //查询全部套餐
    @GetMapping("/mealall")
    public R ListAllMeal() {
        List<Meal> list = mealService.list(null);
        return new R(200, "查询成功", list);
    }

    //查询单个套餐(根据id查询)
    @PostMapping("/mealforid")
    public R ListForName(@RequestParam String mealId) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("meal_id", mealId);
        List<Meal> meals = mealService.listByMap(map);
        return new R(200, "查询成功", meals);
    }

    //根据id批量删除套餐
    @DeleteMapping("/deleteforid")
    public R DeleteForName(@RequestBody JSONObject mealList){
        JSONArray MealIdList = mealList.getJSONArray("MealIdList");

        if (mealService.removeBatchByIds(MealIdList)) {
            return new R(200, "删除成功");
        } else return new R(0, "删除失败");
    }


    //添加课程至套餐中
    @PostMapping("/addmealcourese")
    public R AddMealCourse(
            @RequestParam String mealId,
            @RequestParam String courseId,
            @RequestParam String userId
    ) {
        MealCourse mealCourse = new MealCourse();
        mealCourse.setMealId(mealId);
        mealCourse.setCourseId(courseId);
        mealCourse.setUserId(userId);
        LocalDateTime time = LocalDateTime.now();
        mealCourse.setCreateTime(time);



        if (mealCourseService.saveOrUpdate(mealCourse)) {
            return new R(200, "添加成功");
        } else return new R(0, "添加失败");
    }

    //查询套餐中的课程(根据套餐id)
    @PostMapping ("/mealcourseall")
    public  R ListAllMealCourse(@RequestParam String mealId){
        List<Course> course =new ArrayList<>();
        HashMap<String, Object> map = new HashMap<>();
        map.put("meal_id", mealId);
        List<MealCourse> meals = mealCourseService.listByMap(map);
        for (MealCourse mealCourse:meals){
            String courseId = mealCourse.getCourseId();
            Course byId = courseService.getById(courseId);
            course.add(byId);
        }


//        Meal byId = mealService.getById(mealId);
//        String mealId1 = byId.getMealId();
//        HashMap<String, Object> map = new HashMap<>();
//        map.put("meal_id", mealId1);
//        List<MealCourse> meals = mealCourseService.listByMap(map);
        return new R(200,"查询成功",course);
    }

    //批量删除课程(在套餐中)
    @DeleteMapping("/deleteforcourseid")
    public R DeleteForCourseId(@RequestBody JSONObject  mealCoursesList) {
        Object mealMealId = mealCoursesList.get("MealId");

        for (Object ttt : mealCoursesList.getJSONArray("MealCoursesList")) {

            QueryWrapper<MealCourse> wrapper = Wrappers.query();
            wrapper.eq("meal_id",mealMealId);
            wrapper.eq("course_id",ttt.toString());
            mealCourseService.remove(wrapper);
        }
        return new R(200, "删除成功");
    }



//   批量添加课程到套餐中
    @PostMapping("/batchaddaealcourse")
    public  R BatchAddMealCourse(@RequestBody JSONObject list){
        List<MealCourse> mealCourses =new ArrayList<>();
        Object mealId = list.get("mealId");
        Object userId = list.get("userId");
        Object sortorder = list.get("sortorder");
        for (Object ttt :list.getJSONArray("courseId")){
            MealCourse mealCourse =new MealCourse();
            mealCourse.setMealId((String) mealId);
            mealCourse.setUserId((String) userId);
            mealCourse.setSort_order((String) sortorder);
            mealCourse.setCourseId(ttt.toString());
            mealCourses.add(mealCourse);
        }
        mealCourseService.saveBatch(mealCourses);

        return new R(200, "添加成功");

    }

    //通过课程id返回课程详情
    @PostMapping("/researchdetailbyid")
    public  R ResearchDetailById(@RequestParam String mealid){
        Meal Id = mealService.getById(mealid);
        String mealDetail = Id.getMealDetail();
        return new R(200,"查询成功",mealDetail);
    }

//    分页查询
    @PostMapping("/PageMealCourse")
    public  R PageMealCourse(@RequestParam int current ,@RequestParam int size,
                             @RequestParam String mealId ){
        Page<MealCourse> userPage = new Page<>(current, size);
        QueryWrapper<MealCourse> wrapper = new QueryWrapper<>();
        wrapper.eq("meal_id",mealId);
        Page<MealCourse> pageList =mealCourseMapper.selectPage(userPage, wrapper);
        List<MealCourse> records = pageList.getRecords();
        return new R(200,"查询成功",records);

    }


    //计算课程总数量
    @PostMapping("/GetCourseAmount")
    public  R GetCourseAmount(@RequestParam String mealId){
        List<Course> course =new ArrayList<>();
        HashMap<String, Object> map = new HashMap<>();
        map.put("meal_id", mealId);
        List<MealCourse> meals = mealCourseService.listByMap(map);
        int num = 0 ;
        for (MealCourse mealCourse:meals){
            num++;

        }
        String str =Integer.toString(num);
        Meal meal =new Meal();
        meal.setMealId(mealId);
        meal.setTotalCourse(str);
        mealeMapper1.updateById(meal);

        List<Meal> meals2 = mealService.listByMap(map);
        return new R(200, "查询成功", meals2);

    }

    //修改套餐详情
    @GetMapping ("/UpdateMealDetail")
    public R UpdateMealDetail(@RequestParam String mealId,@RequestParam String mealDetail,
                              @RequestParam String mealName,@RequestParam String Intruduction,
                              @RequestParam String ImgUrl){
        Meal meal =new Meal();
        meal.setMealId(mealId);
        meal.setMealName(mealName);
        meal.setIntrduction(Intruduction);
        meal.setMealDetail(mealDetail);
        meal.setMealImgUrl(ImgUrl);
         mealService.updateById(meal);
        return new R(200,"修改成功");
    }


    //修改套餐对应的课程
    @GetMapping("/UpdateCourseFromMeal")
    public R UpdateCourseFromMeal(@RequestParam String id ,
                              @RequestParam String courseid,
                              @RequestParam String mealid,
                              @RequestParam String sortorder){
        MealCourse mealCourse = new MealCourse();
        mealCourse.setId(id);
        mealCourse.setCourseId(courseid);
        mealCourse.setMealId(mealid);
        mealCourse.setSort_order(sortorder);
        mealCourseService.updateById(mealCourse);
        return  new R(200,"修改成功");
    }





}
