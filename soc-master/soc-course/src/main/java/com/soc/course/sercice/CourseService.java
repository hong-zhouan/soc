package com.soc.course.sercice;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.soc.course.entity.Course;


import java.util.List;

public interface CourseService extends IService<Course> {

    List<Course> getCourseListByIds(List<String> courseIds);

    boolean updateCourse(Course course);

    boolean deleteCourses(List<String> courseIds);

    boolean addCourse(Course course);

    boolean deleteCourseById(String couseId);

    IPage<Course> getPage(int currentPage, int pageSize);
}
