package com.soc.course.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

@Data
public class Meal {
    @TableId(type = IdType.ASSIGN_ID)
    private String mealId;

    private String mealName;
    private String mealImgUrl;
    private String mealDetail;
    private String intrduction;

    private String creatTeacher;
    private String totalCourse;

    private Boolean isPublic;
    // getters and setters
}
