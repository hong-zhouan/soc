package com.soc.course.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.soc.course.entity.Meal;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MealMapper extends BaseMapper<Meal> {
}

