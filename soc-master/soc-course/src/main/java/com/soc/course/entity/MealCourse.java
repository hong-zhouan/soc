package com.soc.course.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.sql.Date;
import java.time.LocalDateTime;

@Data
public class MealCourse {
    @TableId(type = IdType.ASSIGN_ID)
    private String id;

    private String mealId;
    private String courseId;
    private String userId;
    private LocalDateTime createTime;
    private String sort_order;

    // getters and setters
}
