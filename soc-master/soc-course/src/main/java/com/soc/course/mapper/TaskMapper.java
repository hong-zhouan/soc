package com.soc.course.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.soc.course.entity.Task;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface TaskMapper extends BaseMapper<Task> {
}

