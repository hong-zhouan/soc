package com.soc.course.sercice.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.soc.course.entity.CourseUser;
import com.soc.course.mapper.CourseUserMapper;
import com.soc.course.sercice.CourseUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CourseUserServiceImpl extends ServiceImpl<CourseUserMapper, CourseUser> implements CourseUserService {


    @Autowired
    private CourseUserMapper courseUserMapper;

    /**
     * @Author hjy
     * @Description //TODO  创建课程(学生添加课程,老师创建课程)
     * @Date 1:32 2023/4/23
     * @Param userId: 用户id
     * @Param courseId:
     * @Param classId:
     * @Param type:
     * @return: boolean
     **/
    @Override
    public boolean addUserCourse(String userId, String courseId, String classId, Integer type) {
        CourseUser courseUser = new CourseUser();
        courseUser.setUserId(userId);
        courseUser.setCourseId(courseId);
        courseUser.setClassId(classId);
        courseUser.setType(type);
        return save(courseUser);
    }

    @Override
    public boolean deleteUserCourseById(String id) {
        return removeById(id);
    }

    @Override
    public boolean addBatchUserCourse(List<CourseUser> courseUserList) {
        return saveBatch(courseUserList);
    }

    /**
     * @Author hjy
     * @Description //TODO 课程绑定班级
     * @Date 1:34 2023/4/23
     * @Param studentIds: 学生id列表
     * @Param ClassId: 班级id
     * @return: boolean
     **/
    @Override
    public boolean courseAddStudents(List<String> studentIds, String ClassId, String courseId) {

        List<CourseUser> courseUsers = new ArrayList<CourseUser>();
        for (String studentId : studentIds) {
            CourseUser courseUser = new CourseUser();
            courseUser.setUserId(studentId);
            courseUser.setClassId(ClassId);
            courseUser.setType(1);
            courseUser.setCourseId(courseId);
            courseUsers.add(courseUser);
        }
        return saveBatch(courseUsers);
    }

    @Override
    public List<String> getCourseIdsByUser(String userId, Integer type) {
        List<CourseUser> courseUsers = courseUserMapper.selectList(new QueryWrapper<CourseUser>()
                .eq("user_id", userId)
                .eq("type", type)
        );
        return courseUsers.stream()
                .map(CourseUser::getCourseId)
                .collect(Collectors.toList());
    }

    @Override
    public boolean deleteUserCourseByCourseId(String courseId) {
        return remove(new QueryWrapper<CourseUser>()
                .eq("course_id", courseId));
    }
}
