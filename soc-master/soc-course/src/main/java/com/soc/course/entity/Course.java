package com.soc.course.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class Course {
    @TableId(type = IdType.ASSIGN_ID)
    private String courseId;
    private String courseName;
    private String courseTeacher;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date createTime=new Date();;
    private String courseImg;
    @TableField(exist = false)
    private String value;
    private String detail;
    private String university;
    private Boolean isPublic;
    // getters and setters

    public String getValue() {
        return courseName;
    }
//    {courseName:"test",university:"北京大学",courseType:"t","courseImg":"http://test.com",detail:"sgsgsgsggsgs"}
}
