package com.soc.course.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.soc.course.entity.Course;
import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface CourseMapper extends BaseMapper<Course> {
}

