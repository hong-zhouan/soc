package com.soc.course.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.soc.course.entity.MealCourse;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface MealCourseMapper extends BaseMapper<MealCourse> {
    Boolean batchInsert(List<MealCourse> userList);
}

