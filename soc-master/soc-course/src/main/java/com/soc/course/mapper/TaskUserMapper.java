package com.soc.course.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.soc.course.entity.TaskUser;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface TaskUserMapper extends BaseMapper<TaskUser> {
}

