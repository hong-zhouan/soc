package com.soc.course.sercice.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.soc.course.entity.Meal;
import com.soc.course.mapper.MealMapper;
import com.soc.course.sercice.MealService;
import org.springframework.stereotype.Service;

@Service
public class MealServiceImpl extends ServiceImpl<MealMapper, Meal> implements MealService {

}
