package com.soc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SocCourseApplication {

    public static void main(String[] args) {
        SpringApplication.run(SocCourseApplication.class, args);
    }

}
