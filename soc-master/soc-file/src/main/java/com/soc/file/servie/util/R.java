package com.soc.file.servie.util;

import lombok.Data;
import org.apache.http.HttpStatus;

import java.util.ArrayList;

@Data
public class R {
	private Integer code;
	private String msg="";
	private Object data=new ArrayList<>();

	public R() {

	}

	public R(Integer code) {

		this.code = HttpStatus.SC_OK;
	}

	public R(Integer code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public R(Integer code, Object data) {
		this.code = code;
		this.msg="";
		this.data =data;
	}
	public R(Integer code, String msg, Object data) {
		this.code = code;
		this.msg = msg;
		this.data = data;
	}

	 public static R ok (String msg ,Object data){
		 R r = new R();
		 r.setCode(HttpStatus.SC_OK);
		 r.setMsg(msg);
		 r.setData(data);

		 return (r);
	 }

	public static R error(String msg,Object data) {
		R r = new R();
		r.setCode(HttpStatus.SC_INTERNAL_SERVER_ERROR);
		r.setMsg("上传失败");
		r.setData("无");

		return r;

	}
}















//package com.wusuowei.utils;
//
//		import com.alibaba.fastjson.JSON;
//		import com.alibaba.fastjson.TypeReference;
//		import org.apache.http.HttpStatus;
//
//		import java.util.HashMap;
//		import java.util.Map;
//
///**
// * 返回数据
// *
// * @author Mark sunlightcs@gmail.com
// */
//public class R extends HashMap<String, Object> {
//	private static final long serialVersionUID = 1L;
//
//	public R setData(Object data) {
//		put("data",data);
//		return this;
//	}
//
//	//利用fastjson进行反序列化
//	public <T> T getData(TypeReference<T> typeReference) {
//		Object data = get("data");	//默认是map
//		String jsonString = JSON.toJSONString(data);
//		T t = JSON.parseObject(jsonString, typeReference);
//		return t;
//	}
//
//	public R() {
//		put("code", HttpStatus.SC_OK);
//		put("msg", "success");
//
//	}
//
//	public static R error() {
//		return error(HttpStatus.SC_INTERNAL_SERVER_ERROR, "未知异常，请联系管理员");
//	}
//
//	public static R error(String msg) {
//		return error(HttpStatus.SC_INTERNAL_SERVER_ERROR, msg);
//	}
//
//	public static R error(int code, String msg) {
//		R r = new R ();
//		r.put("code", code);
//		r.put("msg", msg);
//		return r;
//	}
//
//	public static R ok(String msg) {
//		R r = new R();
//		r.put("msg", msg);
//		return r;
//	}
//
//	public static R ok(Map<String, Object> map) {
//		R r = new R();
//		r.putAll(map);
//		return r;
//	}
//
//	public static R ok() {
//		return new R();
//	}
//
//	public R put(String key, Object value) {
//		super.put(key, value);
//		return this;
//	}
//	public  Integer getCode() {
//
//		return (Integer) this.get("code");
//	}
//
//}
