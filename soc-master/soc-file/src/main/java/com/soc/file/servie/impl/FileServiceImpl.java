package com.soc.file.servie.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.soc.file.bean.FilePO;
import com.soc.file.dao.FileMapper;
import com.soc.file.servie.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

@Service
public class FileServiceImpl extends ServiceImpl<FileMapper,FilePO> implements FileService {
    @Autowired
    private FileMapper fileMapper;

    @Override
    public Integer addFile(FilePO filePO) {
        return fileMapper.insertFile(filePO);
    }

    @Override
    public Boolean selectFileByMd5(String md5) {
        FilePO filePO = fileMapper.selectFileByMd5(md5);
        return filePO != null;
    }

    @Override
    public List<FilePO> selectFileList() {
        List<FilePO> list = fileMapper.selectFileList();
        return  list;
    }

    @Override
    public FilePO selectFileNameByMd5(String md5) {
        FilePO filePO = fileMapper.selectFileNameByMd5(md5);
         return filePO;

    }

    @Override
    public void deleteFileByMd5(String md5) {
        fileMapper.deleteFileByMd5(md5);
    }


    @Override
    public boolean saveBatch(Collection<FilePO> entityList, int batchSize) {
        return false;
    }

    @Override
    public boolean saveOrUpdateBatch(Collection<FilePO> entityList, int batchSize) {
        return false;
    }

    @Override
    public boolean updateBatchById(Collection<FilePO> entityList, int batchSize) {
        return false;
    }

    @Override
    public boolean saveOrUpdate(FilePO entity) {
        return false;
    }

    @Override
    public List<FilePO> listByMap(Map<String, Object> columnMap) {
        return FileService.super.listByMap(columnMap);
    }

    @Override
    public FilePO getOne(Wrapper<FilePO> queryWrapper, boolean throwEx) {
        return null;
    }

    @Override
    public Map<String, Object> getMap(Wrapper<FilePO> queryWrapper) {
        return null;
    }

    @Override
    public <V> V getObj(Wrapper<FilePO> queryWrapper, Function<? super Object, V> mapper) {
        return null;
    }

    @Override
    public FileMapper getBaseMapper() {
        return null;
    }

    @Override
    public Class<FilePO> getEntityClass() {
        return null;
    }


}



