package com.soc.file.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.soc.file.bean.FilePO;
import org.apache.ibatis.annotations.Mapper;


import java.util.List;

@Mapper
public interface FileMapper extends BaseMapper<FilePO> {
    public Integer insertFile(FilePO filePO) ;

    FilePO selectFileByMd5(String md5);

    List<FilePO> selectFileList();


     FilePO selectFileNameByMd5(String md5);

    void deleteFileByMd5(String md5);
}
