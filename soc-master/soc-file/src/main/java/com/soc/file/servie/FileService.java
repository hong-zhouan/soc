package com.soc.file.servie;


import com.baomidou.mybatisplus.extension.service.IService;
import com.soc.file.bean.FilePO;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public interface FileService extends IService<FilePO> {
    Integer addFile(FilePO filePO);

    Boolean selectFileByMd5(String md5);

    List<FilePO> selectFileList();


    FilePO selectFileNameByMd5(String md5);


    void deleteFileByMd5(String md5);


}
