package com.soc.auth.controller;

import com.soc.auth.domain.entity.User;
import com.soc.auth.mapper.UserMapper;
import com.soc.auth.ratelimit.RateLimit;
import com.soc.auth.utils.CurrentUserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RateLimit(qps = 45)
@RequestMapping("/test")
public class TestController {
    @Autowired
    private UserMapper userMapper;


    // @RateLimit(qps = 15)
    @GetMapping ("/test1")
    public String test1() {

        User currentUser = CurrentUserUtil.getCurrentUser();
        return "test1!!";
    }
    @GetMapping ("/test2")
    public String test2() {

        List<String> courseIdByClassId = userMapper.getCourseIdByClassId("1729831161510768642");
        System.out.println(courseIdByClassId.toString());
        return "test2!!";
    }

}
