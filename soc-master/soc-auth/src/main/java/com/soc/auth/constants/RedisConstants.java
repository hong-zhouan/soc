package com.soc.auth.constants;

public class RedisConstants {

    public static final Long CACHE_EXAM_PAPER_TTL = 30L;

    public static final String LOGIN_USER_KEY = "soc:login:";

    public static final String CAPTCHA_KEY = "soc:captcha:";

    public static final Long CAPTCHA_TTL = 60L;

}
