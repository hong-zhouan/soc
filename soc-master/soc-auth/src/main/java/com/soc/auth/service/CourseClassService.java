package com.soc.auth.service;

import com.soc.auth.domain.entity.CourseClass;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author HongZhouAn
 * @since 2023-06-25
 */
public interface CourseClassService extends IService<CourseClass> {

}
