package com.soc.auth.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.soc.auth.domain.entity.TeacherClass;

public interface TeacherClassService extends IService<TeacherClass> {

}
