package com.soc.auth.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.soc.auth.domain.dto.CertificationDTO;
import com.soc.auth.domain.dto.StudentRegisterDTO;
import com.soc.auth.domain.dto.UserForgetDTO;
import com.soc.auth.domain.dto.UserUpdateDTO;
import com.soc.auth.domain.entity.School;
import com.soc.auth.domain.entity.User;
import com.soc.auth.domain.vo.RouterVo;
import com.soc.auth.domain.vo.UserLoginVO;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author HongZhouAn
 * @since 2023-06-25
 */
public interface UserService extends IService<User> {

    UserLoginVO getLoginSysUserInfo();

    List<RouterVo> getRouters();

    boolean registerStudent(StudentRegisterDTO studentRegisterDTO);

    boolean updateCurrentUserInfo(UserUpdateDTO userUpdateDTO);

    boolean certification(CertificationDTO certificationDTO);

    boolean forgetPassword(UserForgetDTO userForgetDTO);

}
