package com.soc.auth.domain.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class JoinClassDTO {

    List<StudentForm> studentFormList;

    @NotNull(message = "班级id不能为空")
    Long classId;

}



