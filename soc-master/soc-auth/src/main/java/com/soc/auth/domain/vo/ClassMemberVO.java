package com.soc.auth.domain.vo;

import lombok.Data;

@Data
public class ClassMemberVO {

    private String id;

    private String no;

    private String realName;

    /**
     * 是否实名（学生专属）（0：代表未实名，1：代表已实名）
     */
    private Integer isCertification;

    /**
     * 用户任务点完成情况
     **/
    private String userTaskCount;
}
