package com.soc.auth.domain.dto;

import lombok.Data;

@Data
public class ClassCreateDTO {

    String courseId;

    String name;

}
