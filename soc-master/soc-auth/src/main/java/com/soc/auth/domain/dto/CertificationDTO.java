package com.soc.auth.domain.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class CertificationDTO {

    /**
     * 学号
     */
    @NotBlank(message = "学生学号不能为空")
    private String no;

    /**
     * 学生真实姓名
     */
    @NotBlank(message = "学生真实姓名不能为空")
    private String RealName;

    /**
     * 班级绑定码（需要班级绑定码与一个班级绑定，才算实名）
     */
    @NotBlank(message = "班级绑定码不能为空")
    private String bindCode;

    /**
     * 学校名称
     */
    @NotBlank(message = "学校名称不能为空")
    private String schoolName;

}
