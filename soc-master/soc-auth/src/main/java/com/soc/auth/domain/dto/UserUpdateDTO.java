package com.soc.auth.domain.dto;

import lombok.Data;

@Data
public class UserUpdateDTO {

    /**
     * 密码
     */
    private String password;

    /**
     * 头像
     */
    private String avatar;
    /**
     * 真实姓名
     */
    private String realName;
    /**
     * 学校
     */
    private String school;


}
