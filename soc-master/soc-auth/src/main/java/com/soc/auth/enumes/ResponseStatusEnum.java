package com.soc.auth.enumes;


public enum ResponseStatusEnum {

    OK(200, "成功"),

    AccessTokenError(400, "用户登录令牌失效"),

    UNAUTHORIZED(401, "用户未登录"),

    AuthError(402, "用户登录失败"),

    FORBIDDEN(403, "用户没有权限访问"),

    RESOURCE_NOT_FOUND(404, "请求资源不存在"),

    Error_Req(405, "错误请求"),

    InnerServerError(500, "系统内部错误"),

    ParameterValidError(501, "参数验证错误");

    final int code;

    final String message;

    ResponseStatusEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
