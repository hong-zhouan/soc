package com.soc.auth.domain.vo;

import lombok.Data;

import java.util.List;

@Data
public class UserLoginVO {

    private List<String> permissions;

    private List<String> roles;

    private SysUserInfo user;

    @Data
    public static class SysUserInfo {
        /**
         * id
         */
        private String id;

        /**
         * 手机号
         */
        private String phone;

        /**
         * 学号（学生专属）
         */
        private String no;

        /**
         * 真实姓名
         */
        private String realName;

        /**
         * 头像
         */
        private String avatar;

        /**
         * 学校id
         */
        private String schoolId;

        /**
         * 是否实名（学生专属）（0：代表未实名，1：代表已实名）
         */
        private Integer isCertification;

        /**
         * 个人简介（老师专属）
         */
        private String individualResume;

        private String classId;

    }
}
