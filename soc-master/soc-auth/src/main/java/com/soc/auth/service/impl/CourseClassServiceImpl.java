package com.soc.auth.service.impl;

import com.soc.auth.domain.entity.CourseClass;
import com.soc.auth.mapper.CourseClassMapper;
import com.soc.auth.service.CourseClassService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author HongZhouAn
 * @since 2023-06-25
 */
@Service
public class CourseClassServiceImpl extends ServiceImpl<CourseClassMapper, CourseClass> implements CourseClassService {

}
