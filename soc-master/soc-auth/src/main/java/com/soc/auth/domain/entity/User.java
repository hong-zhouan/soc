package com.soc.auth.domain.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableField;

import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author HongZhouAn
 * @since 2023-06-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("t_user")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 密码
     */
    private String password;

    /**
     * 学号（学生专属）
     */
    private String no;

    /**
     * 真实姓名
     */
    private String realName;

    /**
     * 角色（0管理员，1教师，2学生）
     */
    private Integer role;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 学校id
     */
    private String schoolId;

    /**
     * 学校名称
     */
    private String schoolName;

    /**
     * 学生所在班级的id（学生专属）
     */
    private String classId;

    /**
     * 是否实名（学生专属）（0：代表未实名，1：代表已实名）
     */
    private Integer isCertification;

    /**
     * 个人简介（老师专属）
     */
    private String individualResume;

    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    /**
     * 删除标志（0：代表未删除，1：代表已删除）
     */
    @TableLogic
    private Integer isDeleted;

}
