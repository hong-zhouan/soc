package com.soc.auth.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.soc.auth.domain.dto.RosterForm;
import com.soc.auth.domain.entity.Roster;
import com.soc.auth.exception.BusinessException;
import com.soc.auth.mapper.RosterMapper;
import com.soc.auth.service.RosterService;
import org.springframework.stereotype.Service;

@Service
public class RosterServiceImpl extends ServiceImpl<RosterMapper, Roster> implements RosterService {

    /**
     *修改班级下某个学生的信息
     */
    @Override
    public boolean updateRoster(RosterForm rosterForm) {

        Roster roster = getOne(new LambdaUpdateWrapper<Roster>()
                .eq(Roster::getId, rosterForm.getId())
        );

        if (roster.getIsCertification() == 1) {
            throw new BusinessException("学生已经实名过，不能修改该学生信息");
        }

        Roster newRoster = BeanUtil.copyProperties(rosterForm, Roster.class);
        return updateById(newRoster);
    }

    @Override
    public boolean deleteRoster(String studentId) {

        Roster roster = getOne(new LambdaUpdateWrapper<Roster>()
                .eq(Roster::getId, studentId)
        );

        if (roster.getIsCertification() == 1) {
            throw new BusinessException("学生已经实名过，不能删除该学生信息");
        }

        return removeById(studentId);
    }
}
