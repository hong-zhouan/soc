package com.soc.auth.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.soc.auth.domain.dto.ClassCoursePageQuery;
import com.soc.auth.domain.entity.Class;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author HongZhouAn
 * @since 2023-06-25
 */
@Mapper
public interface ClassMapper extends BaseMapper<Class> {

    List<Class> listClassPagesByCourseIdAndTeacherId(Page<Class> page,
                                                     @Param("queryParams") ClassCoursePageQuery queryParams,
                                                     @Param("teacherId") String teacherId);

}
