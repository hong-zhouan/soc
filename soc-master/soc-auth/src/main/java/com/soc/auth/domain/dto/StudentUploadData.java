package com.soc.auth.domain.dto;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

@Data

public class StudentUploadData implements Cloneable {
    @ExcelProperty("学号")
    private String sno;
    @ExcelProperty("姓名")
    private String studentName;

}
