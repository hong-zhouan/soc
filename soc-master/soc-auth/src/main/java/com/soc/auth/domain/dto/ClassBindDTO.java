package com.soc.auth.domain.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class ClassBindDTO {

    @NotBlank(message = "课程id不能为空")
    String courseId;

    @NotBlank(message = "班级id不能为空")
    String classId;

}
