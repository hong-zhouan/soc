package com.soc.auth.domain.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class UserForgetDTO {

    @NotBlank(message = "手机号不能为空！")
    private  String phone;

    @NotBlank(message = "密码不能为空！")
    private String password;

    @NotBlank(message = "密码不能为空！")
    private String value;
}
