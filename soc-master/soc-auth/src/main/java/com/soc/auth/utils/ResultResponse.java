package com.soc.auth.utils;

import com.soc.auth.enumes.ResponseStatusEnum;
import lombok.Data;

@Data
public class ResultResponse<T> {

    private Integer code;

    private String message;

    private T data;


    public ResultResponse(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public ResultResponse(Integer code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public ResultResponse(ResponseStatusEnum responseStatus) {
        this.code = responseStatus.getCode();
        this.message = responseStatus.getMessage();
        this.data = null;
    }

    public static <T> ResultResponse<T> success() {
        return success(null);
    }

    public static <T> ResultResponse<T> success(T data) {
        return new ResultResponse<>(ResponseStatusEnum.OK.getCode(), ResponseStatusEnum.OK.getMessage(), data);
    }

    public static <T> ResultResponse<T> success(String message, T data) {
        return new ResultResponse<>(ResponseStatusEnum.OK.getCode(), message, data);
    }

    public static <T> ResultResponse<T> fail() {
        return new ResultResponse<>(ResponseStatusEnum.InnerServerError.getCode(), ResponseStatusEnum.InnerServerError.getMessage(), null);
    }

    public static <T> ResultResponse<T> fail(T data) {
        return new ResultResponse<>(ResponseStatusEnum.InnerServerError.getCode(), ResponseStatusEnum.InnerServerError.getMessage(), data);
    }

    public static <T> ResultResponse<T> fail(ResponseStatusEnum responseStatusEnum) {
        return new ResultResponse<>(responseStatusEnum.getCode(), responseStatusEnum.getMessage(), null);
    }

    public static <T> ResultResponse<T> fail(ResponseStatusEnum responseStatusEnum, String message) {
        return new ResultResponse<>(responseStatusEnum.getCode(), message, null);
    }

    public static <T> ResultResponse<T> fail(String message) {
        return new ResultResponse<>(ResponseStatusEnum.InnerServerError.getCode(), message, null);
    }


    public static <T> ResultResponse<T> response(ResponseStatusEnum responseStatusEnum) {
        return new ResultResponse<>(responseStatusEnum.getCode(), responseStatusEnum.getMessage());
    }

    public static <T> ResultResponse<T> judge(boolean status) {
        if (status) {
            return success();
        } else {
            return fail();
        }
    }
}
