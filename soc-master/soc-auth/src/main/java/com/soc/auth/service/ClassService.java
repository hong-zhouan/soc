package com.soc.auth.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.soc.auth.domain.entity.Class;
import com.soc.auth.domain.vo.ClassMemberVO;
import com.soc.auth.domain.vo.ClassVO;
import com.soc.auth.domain.vo.PageVO;
import com.soc.auth.domain.dto.*;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author HongZhouAn
 * @since 2023-06-05
 */
public interface ClassService extends IService<Class> {

    ClassVO createClass(ClassCreateDTO classCreateDTO);

    boolean joinClassBatch(JoinClassDTO joinClassDTO);

    PageVO<ClassVO> listClassPagesBySchoolId(ClassSchoolPageQuery queryParams, String schoolId);

    PageVO<ClassVO> listClassPages(ClassTeacherPageQuery queryParams);

    PageVO<ClassMemberVO> listClassMemberPages(ClassMemberPageQuery queryParams);

    boolean bindCourse(ClassBindDTO classBindDTO);

    PageVO<ClassVO> listClassPagesByCourseId(ClassCoursePageQuery queryParams, String teacherId);

    boolean updateClassInfo(ClassUpdateDTO classUpdateDTO);

    boolean removeClassByIds(List<String> classIds);
}
