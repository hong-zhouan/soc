package com.soc.auth.exception;

import org.springframework.security.core.AuthenticationException;

/**
 * 验证码输入错误时的异常
 */
public class BadCaptchaException extends AuthenticationException {

    public BadCaptchaException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public BadCaptchaException(String msg) {
        super(msg);
    }
}
