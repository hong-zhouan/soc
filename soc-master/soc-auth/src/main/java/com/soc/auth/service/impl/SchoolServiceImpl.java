package com.soc.auth.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.soc.auth.domain.entity.School;
import com.soc.auth.mapper.SchoolMapper;
import com.soc.auth.service.SchoolService;
import org.springframework.stereotype.Service;

/**
 * @ClassName SchoolServiceImpl
 * @Description ToDo
 * @Author hjy
 * @Date 2023/7/16 17:00
 **/
@Service
public class SchoolServiceImpl extends ServiceImpl<SchoolMapper, School> implements SchoolService {
}
