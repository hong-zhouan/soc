package com.soc.auth.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = false)
@TableName("t_roster")
public class Roster implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 学号
     */
    private String no;

    /**
     * 学生姓名
     */
    private String realName;

    /**
     * 班级id
     */
    private Long classId;

    /**
     * 是否实名（学生专属）（0：代表未实名，1：代表已实名）
     */
    private Integer isCertification=0;
}
