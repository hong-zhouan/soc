package com.soc.auth.domain.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class ClassUpdateDTO {

    /**
     * 班级名称
     */
    @NotBlank(message = "班级名称不能为空")
    private String name;

    /**
     * 班级id
     */
    @NotNull(message = "班级id不能为空")
    private Long classId;
}
