package com.soc.auth.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.soc.auth.domain.entity.TeacherClass;
import com.soc.auth.mapper.TeacherClassMapper;
import com.soc.auth.service.TeacherClassService;
import org.springframework.stereotype.Service;

@Service
public class TeacherClassServiceImpl extends ServiceImpl<TeacherClassMapper, TeacherClass> implements TeacherClassService {
}
