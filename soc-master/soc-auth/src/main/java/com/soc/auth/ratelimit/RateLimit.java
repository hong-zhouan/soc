package com.soc.auth.ratelimit;

import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.concurrent.TimeUnit;

/**
 * 创建对接口进行限流的注解
 */
@Target({ElementType.METHOD, //标注在方法上，表明对该方法进行限流
        ElementType.TYPE //标注在类上，表明对该类下的所有方法进行限流，当方法与类上都有注解时，默认以方法为准
})
@Retention(RetentionPolicy.RUNTIME)
public @interface RateLimit {

    /**
     * qps (每秒并发量),默认为10，代表1秒处理的请求为10个
     */
    @AliasFor("qps") double value() default 10;

    /**
     * qps (每秒并发量),默认为10，代表1秒处理的请求为10个
     */
    @AliasFor("value") double qps() default 10;

    /**
     * 超时时长,默认不等待
     */
    int timeout() default 0;

    /**
     * 超时时间单位,默认毫秒
     */
    TimeUnit timeUnit() default TimeUnit.MICROSECONDS;
}
