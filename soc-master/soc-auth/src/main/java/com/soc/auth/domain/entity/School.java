package com.soc.auth.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @ClassName School
 * @Description ToDo
 * @Author hjy
 * @Date 2023/7/16 16:58
 **/
@Data
@TableName("t_school")
public class School {

    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    private String schoolName;
}

