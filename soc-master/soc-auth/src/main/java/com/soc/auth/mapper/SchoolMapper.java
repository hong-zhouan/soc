package com.soc.auth.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.soc.auth.domain.entity.School;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author HongZhouAn
 * @since 2023-06-25
 */
@Mapper
public interface SchoolMapper extends BaseMapper<School> {

}
