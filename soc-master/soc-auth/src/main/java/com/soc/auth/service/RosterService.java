package com.soc.auth.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.soc.auth.domain.dto.RosterForm;
import com.soc.auth.domain.entity.Roster;

public interface RosterService extends IService<Roster> {

    boolean updateRoster(RosterForm rosterForm);

    boolean deleteRoster(String studentId);
}
