package com.soc.auth.domain.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class ClassTeacherPageQuery {

    /**
     * 页码
     */
    @NotNull(message = "页码不能为空")
    private int pageNum = 1;

    /**
     * 每页记录数
     */
    @NotNull(message = "每页记录数不能为空")
    private int pageSize = 10;

    /**
     * 创建者id（老师id）
     */
    private String creatorId;

    /**
     * 班级名称可作为查询条件
     */
    private String name;

}
