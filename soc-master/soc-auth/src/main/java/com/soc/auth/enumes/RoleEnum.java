package com.soc.auth.enumes;

import java.util.HashMap;
import java.util.Map;

public enum RoleEnum {

    ADMIN(0, "ADMIN"),

    TEACHER(1, "TEACHER"),

    STUDENT(2, "STUDENT");

    private final int code;

    private final String message;

    RoleEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    private static final Map<Integer, RoleEnum> keyMap = new HashMap<>();

    static {
        for (RoleEnum item : RoleEnum.values()) {
            keyMap.put(item.getCode(), item);
        }
    }

    public static RoleEnum fromCode(Integer code) {
        return keyMap.get(code);
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String getRoleName() {
        return "ROLE_" + message;
    }
}
