package com.soc.auth.controller;

import com.soc.auth.domain.dto.CertificationDTO;
import com.soc.auth.domain.dto.StudentRegisterDTO;
import com.soc.auth.domain.dto.UserForgetDTO;
import com.soc.auth.domain.dto.UserUpdateDTO;
import com.soc.auth.domain.vo.MetaVo;
import com.soc.auth.domain.vo.RouterVo;
import com.soc.auth.domain.vo.UserLoginVO;
import com.soc.auth.service.UserService;
import com.soc.auth.utils.ResultResponse;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/user")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/getInfo")
    public Map<String, Object> getLoginUserInfo() {
        UserLoginVO userLoginVO = userService.getLoginSysUserInfo();
        Map<String, Object> result = new HashMap<>();
        result.put("code", 200);
        result.put("msg", "操作成功");
        result.put("permissions", userLoginVO.getPermissions());
        result.put("roles", userLoginVO.getRoles());
        result.put("users", userLoginVO.getUser());
        //return ResultResponse.success(loginTeacherInfo);
        return result;
    }

    @GetMapping("/getRouters")
    public ResultResponse<List<RouterVo>> getRouters() {

        return ResultResponse.success(userService.getRouters());
    }

    /**
     * 修改当前登录用户信息
     * @param userUpdateDTO 要修改的信息
     * @return 是否修改成功
     */
    @PutMapping("/update")
    public ResultResponse<Boolean> update(@RequestBody UserUpdateDTO userUpdateDTO) {

        boolean result = userService.updateCurrentUserInfo(userUpdateDTO);
        return ResultResponse.judge(result);
    }

    /**
     * 学生进行注册
     * @param studentRegisterDTO 注册信息
     */
    @PostMapping("/registerStudent")
    public ResultResponse<Object> register(@RequestBody @Valid StudentRegisterDTO studentRegisterDTO) {

        boolean result = userService.registerStudent(studentRegisterDTO);
        return ResultResponse.judge(result);
    }

    /**
     * 学生进行实名
     * @param certificationDTO 实名信息
     */
    @PutMapping("/certification")
    public ResultResponse<Boolean> certification(@Valid @RequestBody CertificationDTO certificationDTO) {

        boolean result = userService.certification(certificationDTO);
        return ResultResponse.judge(result);
    }

    @PostMapping("/forgetPassword")
    public ResultResponse<String> forgetPassword(@Valid @RequestBody UserForgetDTO userForgetDTO) throws Exception {

        boolean b = userService.forgetPassword(userForgetDTO);
        return ResultResponse.judge(b);

    }





}
