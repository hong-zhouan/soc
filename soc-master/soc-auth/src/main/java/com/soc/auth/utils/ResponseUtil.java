package com.soc.auth.utils;

import cn.hutool.json.JSONUtil;
import com.soc.auth.enumes.ResponseStatusEnum;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@Slf4j
public class ResponseUtil {


    public static void response(HttpServletResponse response, ResponseStatusEnum responseStatusEnum) {
        response(response, responseStatusEnum.getCode(), responseStatusEnum.getMessage());
    }

    public static void response(HttpServletResponse response, ResponseStatusEnum responseStatusEnum, Object content) {
        response(response, responseStatusEnum.getCode(), responseStatusEnum.getMessage(), content);
    }

    public static void response(HttpServletResponse response, int code, String msg) {
        response(response, code, msg, null);
    }

    public static void response(HttpServletResponse response, int code, String msg, Object content) {
        try {
            ResultResponse<Object> res = new ResultResponse<>(code, msg, content);
            String resStr = JSONUtil.toJsonStr(res);
            response.setContentType("application/json;charset=utf-8");
            assert resStr != null;
            response.getWriter().write(resStr);
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
    }
}
