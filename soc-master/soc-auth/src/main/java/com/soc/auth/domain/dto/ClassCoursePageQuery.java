package com.soc.auth.domain.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class ClassCoursePageQuery {

    /**
     * 页码
     */
    @NotNull(message = "页码不能为空")
    private int pageNum = 1;

    /**
     * 每页记录数
     */
    @NotNull(message = "每页记录数不能为空")
    private int pageSize = 10;

    /**
     * 班级名称作为查询条件
     */
    private String name;

    /**
     * 课程id
     */
    @NotBlank(message = "课程id不能为空")
    private String courseId;

    /**
     * 老师id
     */
    private String teacherId;
}
