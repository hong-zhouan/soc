package com.soc.auth.domain.dto;

import lombok.Data;

@Data
public class StudentForm {

    private String no;

    private String realName;
}
