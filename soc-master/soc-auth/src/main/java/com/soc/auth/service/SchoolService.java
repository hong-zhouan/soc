package com.soc.auth.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.soc.auth.domain.entity.School;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author HongZhouAn
 * @since 2023-06-25
 */
public interface SchoolService extends IService<School> {

}
