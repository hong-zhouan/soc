package com.soc.auth.utils;

import com.soc.auth.domain.entity.User;
import com.soc.auth.security.RedisAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;

public class CurrentUserUtil {

    // 获得当前用户
    public static User getCurrentUser() {
        RedisAuthenticationToken authentication = (RedisAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
        return (User)authentication.getPrincipal();
    }

}
