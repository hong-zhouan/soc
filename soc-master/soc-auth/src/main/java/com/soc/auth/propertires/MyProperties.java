package com.soc.auth.propertires;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "my")
@Component
public class MyProperties {

    private Long jwtTtl;

    public Long getJwtTtl() {
        return jwtTtl;
    }

    public void setJwtTtl(Long jwtTtl) {
        this.jwtTtl = jwtTtl;
    }
}
