package com.soc.auth.domain.dto;

import com.soc.auth.domain.entity.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RedisUser implements Serializable {

    private User user;

    private List<String> roles;

}
