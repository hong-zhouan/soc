package com.soc.auth.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.soc.auth.domain.entity.TeacherClass;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface TeacherClassMapper extends BaseMapper<TeacherClass> {
}
