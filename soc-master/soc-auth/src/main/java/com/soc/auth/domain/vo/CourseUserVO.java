package com.soc.auth.domain.vo;

import lombok.Data;

@Data
public class CourseUserVO {
    private String courseUserId;

    private String userId;

    private String courseId;

    private String classId;

    private Integer type;


}
