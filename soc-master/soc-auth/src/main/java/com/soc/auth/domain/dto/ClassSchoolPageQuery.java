package com.soc.auth.domain.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class ClassSchoolPageQuery {

    /**
     * 页码
     */
    @NotNull(message = "页码不能为空")
    private int pageNum = 1;

    /**
     * 每页记录数
     */
    @NotNull(message = "每页记录数不能为空")
    private int pageSize = 10;
}
