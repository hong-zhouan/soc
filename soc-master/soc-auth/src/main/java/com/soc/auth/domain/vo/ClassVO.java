package com.soc.auth.domain.vo;

import lombok.Data;

@Data
public class ClassVO {

    /**
     * 班级id
     */
    private String id;

    /**
     * 班级名
     */
    private String name;

    /**
     * 绑定码
     */
    private String bindCode;

    /**
     * 班级人数
     */
    private Integer peopleNumber;
}
