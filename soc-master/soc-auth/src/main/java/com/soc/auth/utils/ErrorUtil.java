package com.soc.auth.utils;


public class ErrorUtil {

    // 拼接错误信息的格式
    public static String parameterErrorFormat(String field, String msg) {
        return field + ": " + msg + "; ";
    }
}
