package com.soc.auth.domain.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class StudentRegisterDTO {

    /**
     * 手机号
     */
    @NotBlank(message = "手机号不能为空！")
    private String phone;

    /**
     * 密码
     */
    @NotBlank(message = "密码不能为空！")
    private String password;

    /**
     * 验证码
     */
    @NotBlank(message = "验证码不能为空！")
    private String captcha;

}
