package com.soc.auth.domain.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

@Data
public class RosterForm {

    /**
     * id
     */
    private String id;

    /**
     * 学号
     */
    private String no;

    /**
     * 学生姓名
     */
    private String realName;

}
