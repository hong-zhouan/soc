package com.soc.auth.security;

import com.soc.auth.enumes.ResponseStatusEnum;
import com.soc.auth.utils.ResponseUtil;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 自定义认证失败异常处理器
 */
@Component
public class MyLoginAuthenticationEntryPoint implements AuthenticationEntryPoint {


    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response,
                         AuthenticationException authException) {
        ResponseUtil.response(response, ResponseStatusEnum.UNAUTHORIZED, authException.getMessage());
    }

}
