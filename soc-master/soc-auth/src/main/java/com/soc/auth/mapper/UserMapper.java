package com.soc.auth.mapper;

import com.soc.auth.domain.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.soc.auth.domain.vo.CourseUserVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author HongZhouAn
 * @since 2023-06-25
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {

    boolean addCourseUserList(List<CourseUserVO> courseUserVOS);

    List<String> getCourseIdByClassId(String classId);

}
