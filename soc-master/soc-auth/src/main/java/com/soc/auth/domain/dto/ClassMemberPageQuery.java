package com.soc.auth.domain.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class ClassMemberPageQuery {

    /**
     * 页码
     */
    @NotNull(message = "页码不能为空")
    private int pageNum = 1;

    /**
     * 每页记录数
     */
    @NotNull(message = "每页记录数不能为空")
    private int pageSize = 10;

    /**
     * 学号作为查询条件
     */
    private String no;

    /**
     * 学生姓名作为查询条件
     */
    private String realName;

    /**
     * 班级id
     */
    @NotNull(message = "班级id不能为空")
    private String classId;
    /**
     * 课程id
     **/
    @NotNull(message = "课程id不能为空")
    private String courseId;

}
